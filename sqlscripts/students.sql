CREATE TABLE students (
    id         INTEGER        CONSTRAINT pk_students PRIMARY KEY AUTOINCREMENT
                              NOT NULL,
    first_name VARCHAR (100)  NOT NULL,
    last_name  VARCHAR (100)  NOT NULL,
    seniority  VARCHAR (32)   NOT NULL
                              CONSTRAINT chk_student_seniority CHECK (seniority IN ('Freshman', 'Sophomore', 'Junior', 'Senior')),
    gpa        DECIMAL (3, 2) CONSTRAINT chk_students_gpa CHECK (gpa >= 0.0 AND gpa <= 4.0) 
);

INSERT INTO students
    (id,  first_name, last_name,   seniority,   gpa)
VALUES
    (100, 'John',     'Lennon',    'Junior',    3.4),
    (120, 'Paul',     'McCartney', 'Sophomore', 3.56),
    (150, 'George',   'Harrison',  'Freshman',  2.9),
    (200, 'Ringo',    'Starr',     'Senior',    3.76);
