import os

from csv_extractor import CsvExtractor
from xml_extractor import XmlExtractor
from json_extractor import JsonExtractor
from sql_extractor import SqlExtractor
from excel_extractor import ExcelExtractor


def extractor_factory(filename):
    extractor_mapper = {
        ".xml": XmlExtractor,
        ".csv": CsvExtractor,
        ".json": JsonExtractor,
        ".db": SqlExtractor,
        ".xlsx": ExcelExtractor
    }

    _, ext = os.path.splitext(filename)

    return extractor_mapper.get(ext)
