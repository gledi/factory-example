import openpyxl

from extractor import Extractor
from student import Student


class ExcelExtractor(Extractor):
    def __init__(self, filename):
        self.filename = filename

    def extract(self) -> list[Student]:
        students = []

        workbook = openpyxl.load_workbook(self.filename)
        worksheet = workbook.active
        header = []
        for row in worksheet.rows:
            if not header:
                header = [cell.value for cell in row]
                continue
            vals = [cell.value for cell in row]
            student = Student(**dict(zip(header, vals)))
            students.append(student)

        return students
