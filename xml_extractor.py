import xml.etree.ElementTree as etree

from extractor import Extractor
from student import Student


class XmlExtractor(Extractor):
    def __init__(self, filename):
        self.filename = filename

    def extract(self) -> list[Student]:
        students = []
        tree = etree.parse(self.filename)
        for student_elem in tree.iterfind('./student'):
            attrs = {}
            for prop_elem in student_elem.iterfind('./*'):
                attrs[prop_elem.tag] = prop_elem.text

            student = Student(**attrs)

            students.append(student)

        return students
