import csv

from extractor import Extractor
from student import Student


class CsvExtractor(Extractor):
    def __init__(self, filename):
        self.filename = filename

    def extract(self) -> list[Student]:
        students = []
        with open(self.filename, 'r', encoding='utf-8') as f:
            rd = csv.DictReader(f)
            for row in rd:
                student = Student(**row)
                students.append(student)
        return students
