import sys
import os

from rich.console import Console
from rich.table import Table

from student import Student
from factory import extractor_factory


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    if len(argv) != 1:
        print("No file provided", file=sys.stderr)
        sys.exit(1)

    filename = argv[0]

    if not os.path.exists(filename):
        print("File does not exist", file=sys.stderr)
        sys.exit(1)

    Extractor = extractor_factory(filename)
    if Extractor is None:
        print("We don't support an extractor for this kind of file", file=sys.stderr)
        sys.exit(1)

    extractor = Extractor(filename)
    students = extractor.extract()

    table = Table(title="Students")

    for col in Student.COLUMNS:
        header, attrs = col
        table.add_column(header, **attrs)

    for student in students:
        table.add_row(*student.row)

    console = Console()
    console.print(table)


if __name__ == '__main__':
    sys.exit(main())
