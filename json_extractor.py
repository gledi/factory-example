import json

from extractor import Extractor
from student import Student


class JsonExtractor(Extractor):
    def __init__(self, filename):
        self.filename = filename

    def extract(self) -> list[Student]:
        students = []
        with open(self.filename, 'r', encoding='utf-8') as f:
            entries = json.load(f)
            for entry in entries:
                student = Student(**entry)
                students.append(student)
        return students
