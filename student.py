class Seniority:
    FRESHMAN = "Freshman"
    SOPHOMORE = "Sophomore"
    JUNIOR = "Junior"
    SENIOR = "Senior"

    VALID_SENIORITIES = frozenset([FRESHMAN, SOPHOMORE, JUNIOR, SENIOR])

    @classmethod
    def is_valid(cls, seniority):
        return seniority in cls.VALID_SENIORITIES


class Student:
    COLUMNS = [
        ("ID", {"justify": "right", "style": "blue", "no_wrap": True}),
        ("Student", {"no_wrap": True}),
        ("Seniority", {"no_wrap": True}),
        ("GPA", {"justify": "right", "style": "green", "no_wrap": True}),
    ]

    def __init__(self, id, first_name, last_name, seniority, gpa):
        self.id = int(id) if isinstance(id, str) else id
        self.first_name = first_name
        self.last_name = last_name
        self.seniority = seniority
        self.gpa = float(gpa) if isinstance(gpa, str) else gpa

    @property
    def fullname(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.id}. {self.fullname}'

    def __repr__(self):
        parts = []

        for k, v in self.__dict__.items():
            parts.append(f'{k}={v!r}')

        honors = self.get_honors()
        if honors is not None:
            parts.append(f"honors={honors!r}")

        props = ", ".join(parts)

        return f"{self.__class__.__name__}({props})"

    def get_honors(self):
        if self.gpa >= 3.9:
            return 'Summa Cum Laude'
        elif 3.7 <= self.gpa < 3.9:
            return 'Magna Cum Laude'
        elif 3.5 <= self.gpa < 3.7:
            return 'Cum Laude'

    @property
    def row(self):
        return [
            f"{self.id}.",
            self.fullname,
            self.seniority,
            f'{self.gpa:.2f}'
        ]
