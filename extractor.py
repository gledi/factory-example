from abc import ABC, abstractmethod

from student import Student


class Extractor(ABC):
    @abstractmethod
    def extract(self) -> list[Student]:
        ...
