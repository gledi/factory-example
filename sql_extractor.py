""""
Defines an extractor to extract data from sqlite databases.
"""

import sqlite3

from extractor import Extractor
from student import Student


SQL_STUDENTS = """
SELECT id, first_name, last_name, seniority, gpa
FROM students
ORDER BY id
"""


class SqlExtractor(Extractor):
    """Defines sql extractor class"""
    def __init__(self, filename):
        self.filename = filename

    def extract(self) -> list[Student]:
        """Implements actual extraction logic"""
        students = []
        with sqlite3.connect(self.filename) as conn:
            cur = conn.cursor()
            cur.execute(SQL_STUDENTS)
            cols = [d[0] for d in cur.description]
            for row in cur:
                student = Student(**dict(zip(cols, row)))
                students.append(student)

        return students
